package com.api.tesis.pase.user.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.api.tesis.pase.user.model.UserP;


@Repository
public interface UserRepository extends CrudRepository<UserP, Long> {


}
