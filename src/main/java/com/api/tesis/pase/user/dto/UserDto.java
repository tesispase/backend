package com.api.tesis.pase.user.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UserDto implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String email;
    private int identification;
    private int phone;
    private String type;
}

