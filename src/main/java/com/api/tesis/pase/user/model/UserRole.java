package com.api.tesis.pase.user.model;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

@Data
@Getter
@Setter
@Entity
@Table(name = "user_role", schema = "pase")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_role")
    private Long idUserRole;
    @Column(name = "id_user")
    private int idUser;
    @Column(name = "id_role")
    private int idRole;
    @Column(name = "state")
    private String state;
}
