package com.api.tesis.pase.user.service.impl;


import com.api.tesis.pase.user.model.UserRole;
import com.api.tesis.pase.user.repository.UserRoleRepository;
import com.api.tesis.pase.util.Util;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.api.tesis.pase.user.model.UserP;
import com.api.tesis.pase.user.repository.UserRepository;
import com.api.tesis.pase.user.service.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {
    final private UserRepository userRepository;
    final private UserRoleRepository userRoleRepository;

    public UserServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }


    @Override
    public List<UserP> loadUser(MultipartFile file) throws IOException {
        List<UserP> response = new ArrayList<>();
        if (file == null || file.isEmpty()) {
            return response;
        } else {
            if (!file.getOriginalFilename().equals("usuarios.xls") && !file.getOriginalFilename().equals("usuarios.xlsx")) {
                return null;
            } else {
                Util util = new Util();
                Map<Integer, List<String>> data = util.readExcel(file);
                data.forEach((integer, strings) -> {
                    if (integer > 0) {
                        UserP user = new UserP();
                        UserRole userRole = new UserRole();
                        user.setIdUser(null);
                        user.setNameUser(strings.get(0));
                        user.setLastname(strings.get(1));
                        user.setIdentificationCard(strings.get(2));
                        user.setIdentificationType(strings.get(3));
                        user.setEmail(strings.get(4));
                        user.setUserLogin(strings.get(2));
                        user.setPassword(strings.get(2));
                        user.setState("A");
                        this.userRepository.save(user);
                        userRole.setIdUser(user.getIdUser().intValue());
                        userRole.setIdRole(7);
                        userRole.setState("A");
                        this.userRoleRepository.save(userRole);
                        response.add(user);
                    }
                });
                return response;
            }
        }
    }


}
