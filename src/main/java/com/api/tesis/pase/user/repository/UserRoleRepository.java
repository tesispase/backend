package com.api.tesis.pase.user.repository;

import com.api.tesis.pase.user.model.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Long> {


}
