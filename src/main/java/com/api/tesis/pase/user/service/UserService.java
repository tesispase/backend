package com.api.tesis.pase.user.service;


import org.springframework.web.multipart.MultipartFile;
import com.api.tesis.pase.user.model.UserP;

import java.io.IOException;
import java.util.List;


public interface UserService {

    List<UserP> loadUser(MultipartFile file) throws IOException;
}
