package com.api.tesis.pase.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.api.tesis.pase.user.dto.GenericDto;
import com.api.tesis.pase.user.service.UserService;

import java.io.IOException;

@RestController
@RequestMapping("/User")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class UserController {

    final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @CrossOrigin(origins = "*")
    @PostMapping(path = "/excelImport")
    public ResponseEntity<GenericDto> excelImport(@RequestParam("file") MultipartFile file) throws IOException {
        return ResponseEntity.ok().body(GenericDto.sucess(this.userService.loadUser(file)));
    }
}
