package com.api.tesis.pase.util;

import com.api.tesis.pase.Auth.model.UserPase;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Util {

    public Map<Integer, List<String>>readExcel(MultipartFile file) throws IOException {
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);

        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<String>());
            for (Cell cell : row) {
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            data.get(i).add(cell.getDateCellValue() + "");
                        } else {
                            data.get(i).add(BigInteger.valueOf((long) cell.getNumericCellValue()) + "");
                        }
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        data.get(i).add(cell.getBooleanCellValue() + "");
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        data.get(i).add(cell.getCellFormula() + "");
                        break;
                    default: data.get(new Integer(i)).add(" ");
                }
            }
            i++;
        }
        return data;
    }
}
