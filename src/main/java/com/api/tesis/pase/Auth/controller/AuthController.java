package com.api.tesis.pase.Auth.controller;

import com.api.tesis.pase.Auth.dto.GenericDto;
import com.api.tesis.pase.Auth.dto.UserDto;
import com.api.tesis.pase.Auth.model.UserPase;
import com.api.tesis.pase.Auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class AuthController {

    final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }


    @PostMapping(path = "")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> login(@RequestBody UserDto userDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.authService.login(userDto)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/changePassword")
    public ResponseEntity<GenericDto> changePassword(@RequestBody UserDto userDto) throws NoSuchAlgorithmException {
        UserPase response = this.authService.changePassword(userDto);
        if (response != null) {
            return ResponseEntity.ok().body(GenericDto.sucess(response));
        } else {
            return ResponseEntity.ok().body(GenericDto.sucess("The data does not match with the com.api.tesis.pase.user entered"));
        }
    }
}
