package com.api.tesis.pase.Auth.service.impl;
import com.api.tesis.pase.Auth.dto.UserDto;
import com.api.tesis.pase.Auth.model.UserPase;
import com.api.tesis.pase.Auth.repository.AuthRepository;
import com.api.tesis.pase.Auth.service.AuthService;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;


@Service
public class AuthServiceImpl implements AuthService {
    final private AuthRepository authRepository;

    public AuthServiceImpl(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public UserPase login(UserDto userDto) {
        UserPase response = this.authRepository.findByUserLoginAndPassword(userDto.getUsername(), userDto.getPassword()).orElse(null);
        return response;
    }

    @Override
    public UserPase changePassword(UserDto userDto) throws NoSuchAlgorithmException {
        UserPase response = this.authRepository.findByEmailAndPassword(userDto.getUsername(), userDto.getEmail()).orElse(null);
        if (response != null) {
            response.generarPassword();
            authRepository.save(response);
            return response;
        } else {
            return null;
        }
    }
}
