package com.api.tesis.pase.Auth.service;

import com.api.tesis.pase.Auth.dto.UserDto;
import com.api.tesis.pase.Auth.model.UserPase;
import java.security.NoSuchAlgorithmException;


public interface AuthService {
    UserPase login(UserDto userDto);

    UserPase changePassword(UserDto userDto) throws NoSuchAlgorithmException;

}
