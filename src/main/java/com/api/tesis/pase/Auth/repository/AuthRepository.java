package com.api.tesis.pase.Auth.repository;

import com.api.tesis.pase.Auth.model.UserPase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface AuthRepository extends CrudRepository<UserPase, Long> {

    public Optional<UserPase> findByUserLoginAndPassword(String user, String password);

    public Optional<UserPase> findByEmailAndPassword(String email, String password);


}
