package com.api.tesis.pase.question.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionDto {
    private Long idQuestion;
    private String nameQuestion;
    private String descriptionQuestion;
    private int questionType;
    private String state;
}
