package com.api.tesis.pase.question.repository;

import com.api.tesis.pase.question.model.Question;
import com.api.tesis.pase.question.model.QuestionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface QuestionTypeRepository extends CrudRepository<QuestionType, Long> {
    List<QuestionType> findAllByState(String state);
}