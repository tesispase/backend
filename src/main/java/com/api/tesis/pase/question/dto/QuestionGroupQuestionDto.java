package com.api.tesis.pase.question.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionGroupQuestionDto {
    private Long idQuestionGroupQuestion;
    private int idQuestion;
    private int idQuestionGroup;
    private String state;
}
