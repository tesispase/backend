package com.api.tesis.pase.question.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionGroupDto {
    private Long idQuestionGroup;
    private String nameQuestionGroup;
    private String typeQuestionGroup;
    private String state;
}
