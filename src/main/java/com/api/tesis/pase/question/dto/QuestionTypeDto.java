package com.api.tesis.pase.question.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionTypeDto {
    private Long idQuestionType;
    private String nameQuestionType;
    private String description;
    private String state;
}
