package com.api.tesis.pase.question.repository;

import com.api.tesis.pase.question.model.QuestionGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionGroupRepository extends CrudRepository<QuestionGroup, Long> {

}