package com.api.tesis.pase.question.model;

import com.api.tesis.pase.question.dto.QuestionDto;
import com.api.tesis.pase.question.dto.QuestionTypeDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table(name = "question_type", schema = "pase")
public class QuestionType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_question_type")
    private Long idQuestionType;
    @Column(name = "name_question_type", nullable = false)
    private String nameQuestionType;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "state", nullable = false)
    private String state;

    public QuestionType transformObject(QuestionTypeDto questionTypeDto) {
        QuestionType response = new QuestionType();
        response.setIdQuestionType(questionTypeDto.getIdQuestionType());
        response.setNameQuestionType(questionTypeDto.getNameQuestionType());
        response.setDescription(questionTypeDto.getDescription());
        response.setState(questionTypeDto.getState());
        return response;
    }
}