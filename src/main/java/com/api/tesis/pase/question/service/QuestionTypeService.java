package com.api.tesis.pase.question.service;

import com.api.tesis.pase.question.dto.QuestionTypeDto;
import com.api.tesis.pase.question.model.QuestionType;

import java.util.List;
import java.util.Optional;

public interface QuestionTypeService {
    QuestionType createQuestionType(QuestionTypeDto questionTypeDto);
    QuestionType updateQuestionType(QuestionTypeDto questionTypeDto);
    Optional<QuestionType> searchQuestionType(QuestionTypeDto questionTypeDto);
    QuestionType disabledQuestionType(QuestionTypeDto questionTypeDto);
    List<QuestionType> AllQuestionType();
}
