package com.api.tesis.pase.question.repository;

import com.api.tesis.pase.question.model.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {
    List<Question> findAllByQuestionType(int questionType);
}