package com.api.tesis.pase.question.repository;

import com.api.tesis.pase.question.model.QuestionGroupQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionGroupQuestionRepository extends CrudRepository<QuestionGroupQuestion, Long> {
}