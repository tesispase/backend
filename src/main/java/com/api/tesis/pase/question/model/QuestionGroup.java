package com.api.tesis.pase.question.model;

import com.api.tesis.pase.question.dto.QuestionGroupDto;
import com.api.tesis.pase.question.dto.QuestionTypeDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table(name = "question_group", schema = "pase")
public class QuestionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_question_group")
    private Long idQuestionGroup;
    @Column(name = "name_question_group", nullable = false)
    private String nameQuestionGroup;
    @Column(name = "type_question_group", nullable = false)
    private String typeQuestionGroup;
    @Column(name = "state", nullable = false)
    private String state;

    public QuestionGroup transformObject(QuestionGroupDto questionGroupDto) {
        QuestionGroup response = new QuestionGroup();
        response.setIdQuestionGroup(questionGroupDto.getIdQuestionGroup());
        response.setNameQuestionGroup(questionGroupDto.getNameQuestionGroup());
        response.setTypeQuestionGroup(questionGroupDto.getTypeQuestionGroup());
        response.setState(questionGroupDto.getState());
        return response;
    }
}