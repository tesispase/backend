package com.api.tesis.pase.question.service.impl;

import com.api.tesis.pase.question.dto.QuestionDto;
import com.api.tesis.pase.question.model.Question;
import com.api.tesis.pase.question.repository.QuestionRepository;
import com.api.tesis.pase.question.service.QuestionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {
    final private QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question createQuestion(QuestionDto questionDto) {
        Question response = new Question();
        this.questionRepository.save(response.transformObject(questionDto));
        return response.transformObject(questionDto);
    }

    @Override
    public Question updateQuestion(QuestionDto questionDto) {
        Question response = new Question();
        this.questionRepository.save(response.transformObject(questionDto));
        return response.transformObject(questionDto);
    }

    @Override
    public Optional<Question> searchQuestion(QuestionDto questionDto) {
        return this.questionRepository.findById(questionDto.getIdQuestion());
    }

    @Override
    public List<Question> searchQuestionsOfType(QuestionDto questionDto) {
        return this.questionRepository.findAllByQuestionType(questionDto.getQuestionType());
    }

    @Override
    public Question disabledQuestion(QuestionDto questionDto) {
        Question response = new Question();
        questionDto.setState("I");
        this.questionRepository.save(response.transformObject(questionDto));
        return response.transformObject(questionDto);
    }

}
