package com.api.tesis.pase.question.model;

import com.api.tesis.pase.question.dto.QuestionDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table(name = "question", schema = "pase")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_question")
    private Long idQuestion;
    @Column(name = "name_question", nullable = false)
    private String nameQuestion;
    @Column(name = "description_question", nullable = false)
    private String descriptionQuestion;
    @Column(name = "question_type", nullable = false)
    private int questionType;
    @Column(name = "state", nullable = false)
    private String state;

    public Question transformObject(QuestionDto questionDto){
        Question response = new Question();
        response.setIdQuestion(questionDto.getIdQuestion());
        response.setDescriptionQuestion(questionDto.getDescriptionQuestion());
        response.setNameQuestion(questionDto.getNameQuestion());
        response.setQuestionType(questionDto.getQuestionType());
        response.setState(questionDto.getState());
        return response;
    }
}
