package com.api.tesis.pase.question.controller;
import com.api.tesis.pase.question.dto.QuestionDto;
import com.api.tesis.pase.question.dto.QuestionTypeDto;
import com.api.tesis.pase.question.service.QuestionService;
import com.api.tesis.pase.question.service.QuestionTypeService;
import com.api.tesis.pase.user.dto.GenericDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/Question")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class QuestionController {
    final QuestionService questionService;
    final QuestionTypeService questionTypeService;

    public QuestionController(QuestionService questionService, QuestionTypeService questionTypeService) {
        this.questionService = questionService;
        this.questionTypeService = questionTypeService;
    }


    @CrossOrigin(origins = "*")
    @PostMapping(path = "/create")
    public ResponseEntity<GenericDto> create(@RequestBody QuestionDto questionDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionService.createQuestion(questionDto)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/update")
    public ResponseEntity<GenericDto> update(@RequestBody QuestionDto questionDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionService.updateQuestion(questionDto)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/findQuestion")
    public ResponseEntity<GenericDto> findQuestion(@RequestBody QuestionDto questionDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionService.searchQuestion(questionDto)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/findQuestions")
    public ResponseEntity<GenericDto> findQuestions(@RequestBody QuestionDto questionDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionService.searchQuestionsOfType(questionDto)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/createQuestionType")
    public ResponseEntity<GenericDto> createTypeQuestion(@RequestBody QuestionTypeDto questionTypeDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionTypeService.createQuestionType(questionTypeDto)));
    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/allQuestionType")
    public ResponseEntity<GenericDto> allQuestionType(){
        return ResponseEntity.ok().body(GenericDto.sucess(this.questionTypeService.AllQuestionType()));
    }
}
