package com.api.tesis.pase.question.service;

import com.api.tesis.pase.question.dto.QuestionDto;
import com.api.tesis.pase.question.model.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionService {
    Question createQuestion(QuestionDto questionDto);
    Question updateQuestion(QuestionDto questionDto);
    Optional<Question> searchQuestion(QuestionDto questionDto);
    List<Question> searchQuestionsOfType(QuestionDto questionDto);
    Question disabledQuestion(QuestionDto questionDto);
}
