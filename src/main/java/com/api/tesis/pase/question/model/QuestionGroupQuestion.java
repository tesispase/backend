package com.api.tesis.pase.question.model;

import com.api.tesis.pase.question.dto.QuestionGroupQuestionDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table(name = "question_group_question", schema = "pase")
public class QuestionGroupQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_question_group_question")
    private Long idQuestionGroupQuestion;
    @Column(name = "id_question", nullable = false)
    private int idQuestion;
    @Column(name = "id_question_group", nullable = false)
    private int idQuestionGroup;
    @Column(name = "state", nullable = false)
    private String state;

    public QuestionGroupQuestion transformObject(QuestionGroupQuestionDto questionGroupQuestionDto) {
        QuestionGroupQuestion response = new QuestionGroupQuestion();
        response.setIdQuestionGroupQuestion(questionGroupQuestionDto.getIdQuestionGroupQuestion());
        response.setIdQuestion(questionGroupQuestionDto.getIdQuestion());
        response.setIdQuestionGroup(questionGroupQuestionDto.getIdQuestionGroup());
        response.setState(questionGroupQuestionDto.getState());
        return response;
    }
}