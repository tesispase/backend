package com.api.tesis.pase.question.service.impl;

import com.api.tesis.pase.question.dto.QuestionTypeDto;
import com.api.tesis.pase.question.model.QuestionType;
import com.api.tesis.pase.question.repository.QuestionTypeRepository;
import com.api.tesis.pase.question.service.QuestionTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionTypeServiceImpl implements QuestionTypeService {
    final private QuestionTypeRepository questionTypeRepository;

    public QuestionTypeServiceImpl(QuestionTypeRepository questionTypeRepository) {
        this.questionTypeRepository = questionTypeRepository;
    }

    @Override
    public QuestionType createQuestionType(QuestionTypeDto questionTypeDto) {
        QuestionType response = new QuestionType();
        this.questionTypeRepository.save(response.transformObject(questionTypeDto));
        return response.transformObject(questionTypeDto);
    }

    @Override
    public QuestionType updateQuestionType(QuestionTypeDto questionTypeDto) {
        QuestionType response = new QuestionType();
        this.questionTypeRepository.save(response.transformObject(questionTypeDto));
        return response.transformObject(questionTypeDto);
    }

    @Override
    public Optional<QuestionType> searchQuestionType(QuestionTypeDto questionTypeDto) {
        return this.questionTypeRepository.findById(questionTypeDto.getIdQuestionType());
    }

    @Override
    public List<QuestionType> AllQuestionType() {
        return this.questionTypeRepository.findAllByState("A");
    }
    
    @Override
    public QuestionType disabledQuestionType(QuestionTypeDto questionTypeDto) {
        QuestionType response = new QuestionType();
        questionTypeDto.setState("I");
        this.questionTypeRepository.save(response.transformObject(questionTypeDto));
        return response.transformObject(questionTypeDto);
    }

}
